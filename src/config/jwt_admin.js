const jwt = require('jsonwebtoken')

//自定义密钥
const secret = 'This Wechat applet is from Skygray and Boom of admin';
//过期时间
const expiresIn = '3600s'
//加密方法
const algorithm = "HS256"

// 签发Token;
const token_sign = function (payload) {
    return jwt.sign(payload, secret, {
        algorithm,
        expiresIn
    })
}

//验证token
const token_verify = function (token) {
    return jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            return false;
        } else {
            return decoded;
        }
    });
}

module.exports = {
    token_sign,
    token_verify
}