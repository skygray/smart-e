const mysql = require('mysql');

//创建一个pool连接池
const pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "123456",
    database: "smart_e"
});

const query = (sql, params = null, callback) => {
    pool.getConnection((err, connection) => {
        if (err) {
            callback(err, null);
            throw new Error("数据库连接不成功！" + err);
        } else {
            connection.query(sql, params, (err, results) => {
                callback(err, results);
                connection.release();
            })
        }
    })
}
module.exports = {
    query
};