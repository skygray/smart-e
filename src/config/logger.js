var log4js = require('log4js');
const path = require('path');

log4js.configure({
    appenders: {
        // 控制台输出
        console: {
            type: 'console'
        },
        // 全部日志文件
        app: {
            type: 'file',
            filename: path.join(__dirname, '../../logs/serve'),
            maxLogSize: 1024 * 500, //一个文件的大小，超出后会自动新生成一个文件
            backups: 2, // 备份的文件数量
            pattern: "_yyyy-MM-dd.log",
            alwaysIncludePattern: true,
        },
        // 错误日志文件
        errorFile: {
            type: 'file',
            filename: path.join(__dirname, '../../logs/error'),
            maxLogSize: 1024 * 500, // 一个文件的大小，超出后会自动新生成一个文件
            backups: 2, // 备份的文件数量
            pattern: "_yyyy-MM-dd.log",
            alwaysIncludePattern: true,
        }
    },
    categories: {
        default: {
            appenders: [
                'app',
                'console'
            ],
            level: 'debug'
        },
        error: {
            appenders: ['errorFile'],
            level: 'error'
        },
    },
    replaceConsole: true, // 替换console.log  
});

const logger = function () {
    const logger = log4js.getLogger();
    return logger;
};

module.exports = {
    logger
}