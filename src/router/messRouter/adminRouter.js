const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/messQuery/messQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//获取公共留言板列表(默认降序)
router.get('/messList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_mess_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板列表失败", null));
            logger().error("获取公共留言板列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板列表成功", data));
        }
    });
})

//获取公共留言板列表(升序)
router.get('/messListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_mess_admin_asc, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板列表(升序)失败", null));
            logger().error("获取公共留言板列表(升序)失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板列表(升序)成功", data));
        }
    });
})

//模糊搜索发表者
router.get('/searchMessBord_auther', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_mess_admin_auther, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板搜索发表者列表失败", null));
            logger().error("获取公共留言板搜索发表者列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板搜索发表者列表成功", data));
        }
    });
})

//模糊搜索发表内容
router.get('/searchMessBord_content', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_mess_admin_content, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板搜索发表内容列表失败", null));
            logger().error("获取公共留言板搜索发表内容列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板搜索发表内容列表成功", data));
        }
    });
})

//状态筛选
router.get('/searchMessBord_status', function (req, res) {
    const status = req.query.status;
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_status_admin, [status, count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板状态筛选列表失败", null));
            logger().error("获取公共留言板状态筛选列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板状态筛选列表成功", data));
        }
    });
})

//修改留言状态
router.post('/changeStatus', function (req, res) {
    const messId = req.body.messId;
    const messStatus = req.body.messStatus;
    pool.query(query.change_mess_admin, [messStatus, messId], (error, result) => {
        if (error) {
            res.json(Response.error("修改留言状态失败", null));
            logger().error("修改留言状态失败: " + error);
        } else {
            res.json(Response.success("修改留言状态成功", null));
        }
    });
})

//修改显示层级
router.post('/changeIndex', function (req, res) {
    const messId = req.body.messId;
    const messIndex = req.body.messIndex;
    pool.query(query.select_mess_admin_index, messIndex, (error, result) => {
        if (error) {
            res.json(Response.error("修改显示层级失败", null));
            logger().error("修改显示层级失败: " + error);
        } else {
            const {
                count
            } = JSON.parse(JSON.stringify(result[0]));
            if (count !== 0) {
                res.json(Response.error("修改显示层级失败: 存在相同的层级!", null));
            } else {
                pool.query(query.change_mess_admin_index, [messIndex, messId], (error, result) => {
                    if (error) {
                        res.json(Response.error("修改显示层级失败", null));
                        logger().error("修改显示层级失败: " + error);
                    } else {
                        res.json(Response.success("修改显示层级成功", null));
                    }
                });
            }
        }
    });

})

//删除留言板消息
router.post('/deleteMess', function (req, res) {
    const messId = req.body.messId;
    pool.query(query.delete_mess, messId, (error, result) => {
        if (error) {
            res.json(Response.error("删除留言板消息错误", null));
            logger().error(`删除留言板消息错误: ${error}`);
            return;
        } else {
            res.json(Response.success("删除留言成功", null));
        }
    });
})


module.exports = router;