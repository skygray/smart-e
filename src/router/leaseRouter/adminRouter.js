const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/leaseQuery/leaseQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//获取租赁列表(默认降序)
router.get('/leaseList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_lease_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取租赁列表失败", null));
            logger().error("获取租赁列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取租赁列表成功", data));
        }
    });
})

//获取租赁列表(升序)
router.get('/leaseListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_lease_admin_asc, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取租赁列表(升序)失败", null));
            logger().error("获取租赁列表(升序)失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取租赁列表(升序)成功", data));
        }
    });
})

//模糊搜索租赁标题
router.get('/searchLease_title', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_lease_admin_title, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公告模糊搜索租赁标题列表失败", null));
            logger().error("获取公告模糊搜索租赁标题列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公告模糊搜索租赁标题列表成功", data));
        }
    });
})

//模糊搜索租赁介绍
router.get('/searchLease_describe', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_lease_admin_describe, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公告搜索介绍详细列表失败", null));
            logger().error("获取公告搜索租赁介绍列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公告搜索租赁介绍列表成功", data));
        }
    });
})

//状态筛选
router.get('/searchLease_status', function (req, res) {
    const status = req.query.status;
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_status_admin, [status, count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取租赁状态筛选列表失败", null));
            logger().error("获取租赁状态筛选列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取租赁状态筛选列表成功", data));
        }
    });
})

//修改租赁显示状态
router.post('/changeStatus', function (req, res) {
    const leaseId = req.body.leaseId;
    const leaseStatus = req.body.leaseStatus;
    pool.query(query.change_lease_admin, [leaseStatus, leaseId], (error, result) => {
        if (error) {
            res.json(Response.error("租赁显示状态失败", null));
            logger().error("租赁显示状态失败: " + error);
        } else {
            res.json(Response.success("租赁显示状态成功", null));
        }
    });
})

//修改显示层级
router.post('/changeIndex', function (req, res) {
    const leaseId = req.body.leaseId;
    const leaseIndex = req.body.leaseIndex;
    pool.query(query.select_lease_admin_index, leaseIndex, (error, result) => {
        if (error) {
            res.json(Response.error("修改显示层级失败", null));
            logger().error("修改显示层级失败: " + error);
        } else {
            const {
                count
            } = JSON.parse(JSON.stringify(result[0]));
            if (count !== 0) {
                res.json(Response.error("修改显示层级失败: 存在相同的层级!", null));
            } else {
                pool.query(query.change_lease_admin_index, [leaseIndex, leaseId], (error, result) => {
                    if (error) {
                        res.json(Response.error("修改显示层级失败", null));
                        logger().error("修改显示层级失败: " + error);
                    } else {
                        res.json(Response.success("修改显示层级成功", null));
                    }
                });
            }
        }
    });

})

//修改租赁信息
router.post('/changeInfo', function (req, res) {
    const leaseId = req.body.leaseId;
    const leaseTitle = req.body.leaseTitle;
    const leaseIntroduce = req.body.leaseIntroduce;
    const leasePrice = req.body.leasePrice;
    const leaseContacts = req.body.leaseContacts;
    const leaseTel = req.body.leaseTel;
    const leaseKeyword = req.body.leaseKeyword;
    const leaseIndex = req.body.leaseIndex;
    pool.query(query.select_lease_admin_info, [leaseTitle, leasePrice, leaseIntroduce, leaseContacts, leaseTel, leaseKeyword, leaseIndex, leaseId], (error, result) => {
        if (error) {
            res.json(Response.error("修改租赁信息失败", null));
            logger().error("修改租赁信息失败: " + error);
        } else {
            res.json(Response.success("修改租赁信息成功", null));
        }
    });
})


//删除租赁信息
router.post('/deleteLease', function (req, res) {
    const leaseId = req.body.leaseId;
    pool.query(query.delete_lease, leaseId, (error, result) => {
        if (error) {
            res.json(Response.error("删除租赁信息失败", null));
            logger().error(`删除租赁信息错误: ${error}`);
            return;
        } else {
            res.json(Response.success("删除租赁信息成功", null));
        }
    });
})


module.exports = router;