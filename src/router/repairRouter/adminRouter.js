const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/repairQuery/repairQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//获取维修列表(默认降序)
router.get('/repairList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_repair_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取维修列表失败", null));
            logger().error("获取维修列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取维修列表成功", data));
        }
    });
})

//获取维修列表(升序)
router.get('/repairListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_repair_admin_asc, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取维修列表(升序)失败", null));
            logger().error("获取维修列表(升序)失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取维修列表(升序)成功", data));
        }
    });
})

//模糊搜索维修标题
router.get('/searchRepair_repair_title', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_repair_admin_title, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板搜索维修标题失败", null));
            logger().error("获取公共留言板搜索维修标题失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板搜索维修标题成功", data));
        }
    });
})

//模糊搜索维修描述
router.get('/searchRepair_repair_describe', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_repair_admin_describe, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取公共留言板搜索维修描述失败", null));
            logger().error("获取公共留言板搜索维修描述失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公共留言板搜索维修描述成功", data));
        }
    });
})

//修改维修状态
router.post('/changeStatus', function (req, res) {
    const repairId = req.body.repairId;
    const repairStatus = req.body.repairStatus;
    pool.query(query.change_repair_admin, [repairStatus, repairId], (error, result) => {
        if (error) {
            res.json(Response.error("修改维修状态失败", null));
            logger().error("修改维修状态失败: " + error);
        } else {
            res.json(Response.success("修改维修状态成功", null));
        }
    });
})

//删除维修消息
router.post('/deleteRepair', function (req, res) {
    const repairId = req.body.repairId;
    pool.query(query.delete_repair, repairId, (error, result) => {
        if (error) {
            res.json(Response.error("删除维修消息错误", null));
            logger().error(`删除维修消息错误: ${error}`);
            return;
        } else {
            res.json(Response.success("删除维修消息成功", null));
        }
    });
})


module.exports = router;