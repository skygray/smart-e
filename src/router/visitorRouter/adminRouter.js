const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/visitorQuery/visitorQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//引入scoket通知
const {
    sendSystemMessage_visitor_admin
} = require('../../notice/auditNotice')

//获取访客列表(默认降序) 
router.get('/visitorList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_visitor_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取访客列表失败", null));
            logger().error("获取访客列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取访客列表成功", data));
        }
    });
})

//模糊搜索访客名称
router.get('/searchVisitor', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_visitor_admin_keyWord, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("搜索访客列表失败", null));
            logger().error("搜索访客列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("搜索访客列表成功", data));
        }
    });
})

//获取公共留言板列表(升序)
router.get('/visitorListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_visitorByAsc_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取访客列表失败", null));
            logger().error("获取访客列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取访客列表成功", data));
        }
    });
})

//修改访客审核状态
router.post('/changeStatus', function (req, res) {
    const visitorId = req.body.visitorId;
    const visitorStatus = req.body.visitorStatus;
    pool.query(query.change_visitor_admin, [visitorStatus, visitorId], (error, result) => {
        if (error) {
            res.json(Response.error("修改门禁申请状态失败", null));
            logger().error("修改门禁申请状态失败: " + error);
        } else {
            pool.query(query.select_visitorID_admin, visitorId, (error, result) => {
                if (error) {
                    res.json(Response.error("申请访客登记失败", null));
                    logger().error("申请访客登记失败: " + error);
                    return;
                } else {
                    const {
                        user_id,
                        openid
                    } = JSON.parse(JSON.stringify(result[0]));
                    const time = sd.format(new Date(), 'YYYY-MM-DD HH:mm');
                    sendSystemMessage_visitor_admin(user_id, {
                        openid,
                        handler: "管理员",
                        message: "您有一则访客申请已处理",
                        time,
                        visitorId
                    });
                    res.json(Response.success("修改门禁申请状态成功", null));
                }
            });
        }
    });
})

//删除访客申请
router.post('/delete', function (req, res) {
    const visitorId = req.body.visitorId;
    pool.query(query.delete_visitor_admin, visitorId, (error, result) => {
        if (error) {
            res.json(Response.error("删除访客申请失败", null));
            logger().error("删除访客申请失败: " + error);
        } else {
            res.json(Response.success("删除访客申请成功", null));
        }
    });
})


module.exports = router;