const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/complaintQuery/complaintQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//获取投诉列表(默认降序)
router.get('/complaintList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_complaint_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取投诉列表失败", null));
            logger().error("获取投诉列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取投诉列表成功", data));
        }
    });
})

//获取投诉列表(升序)
router.get('/complaintListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_complaint_admin_asc, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取投诉列表(升序)失败", null));
            logger().error("获取投诉列表(升序)失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取投诉列表(升序)成功", data));
        }
    });
})

//模糊搜索投诉标题
router.get('/searchComplaint_title', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_complaint_admin_title, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取投诉搜索标题列表失败", null));
            logger().error("获取投诉搜索标题列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取投诉搜索标题列表成功", data));
        }
    });
})

//模糊搜索投诉标详细
router.get('/searchComplaint_describe', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_complaint_admin_describe, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取投诉搜索投诉标详细列表失败", null));
            logger().error("获取投诉搜索投诉标详细列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取投诉搜索投诉标详细列表成功", data));
        }
    });
})

//状态筛选
router.get('/searchComplaint_status', function (req, res) {
    const status = req.query.status;
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_status_admin, [status, count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取投诉状态筛选列表失败", null));
            logger().error("获取投诉状态筛选列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取投诉状态筛选列表成功", data));
        }
    });
})

//修改投诉状态
router.post('/changeStatus', function (req, res) {
    const complaintId = req.body.complaintId;
    const complaintStatus = req.body.complaintStatus;
    pool.query(query.change_complaint_admin, [complaintStatus, complaintId], (error, result) => {
        if (error) {
            res.json(Response.error("修改投诉状态失败", null));
            logger().error("修改投诉状态失败: " + error);
        } else {
            res.json(Response.success("修改投诉状态成功", null));
        }
    });
})

//删除投诉
router.post('/deleteComplaint', function (req, res) {
    const complaintId = req.body.complaintId;
    pool.query(query.delete_complaint, complaintId, (error, result) => {
        if (error) {
            res.json(Response.error("删除投诉错误", null));
            logger().error(`删除投诉错误: ${error}`);
            return;
        } else {
            res.json(Response.success("删除投诉成功", null));
        }
    });
})


module.exports = router;