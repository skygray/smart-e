const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/complaintQuery/complaintQuery');

//解析token
const {
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//解析from
const multiparty = require('multiparty')

//发送反馈意见
router.post('/publish', function (req, res) {
    const token = req.headers.authorization;
    const {
        openId
    } = token_verify(token);
    let form = new multiparty.Form() // 设置文件存储路径
    const time = sd.format(new Date(), 'YYYY-MM-DD HH:mm');
    form.parse(req, (err, msg, files) => {
        try {
            const title = msg['title'][0];
            const describe = msg['describe'][0];
            pool.query(query.insert_complaint, [openId, title, describe, time], (error, result) => {
                if (error) {
                    res.json(Response.error("发布投诉信息失败", null));
                    logger().error("发布投诉信息失败: " + error);
                } else {
                    res.json(Response.success("发布投诉信息成功", null));
                }
            });
        } catch (error) {
            res.json(Response.error("发布投诉信息失败", null));
            logger().error("发布投诉信息失败: " + error);
        }
    })
});

module.exports = router;