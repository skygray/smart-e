const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/userQuery/userQuery')

//解析token
const {
    token_sign,
    token_verify
} = require('../../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//引入scoket通知
const {
    sendSystemMessage_owner_admin
} = require('../../notice/auditNotice')


//获取业主申请列表
router.get('/ownerList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_owner_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取业主申请列表失败", null));
            logger().error("获取业主申请列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取业主申请列表成功", data));
        }
    });
})

//搜索业主申请
router.get('/searchOwner', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_owner_admin_keyWord, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("搜索业主申请列表失败", null));
            logger().error("搜索业主申请列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("搜索业主申请列表成功", data));
        }
    });
})

//修改业主申请状态
router.post('/changeOwnerStatus', function (req, res) {
    const ownerId = req.body.ownerId;
    const ownerStatus = req.body.ownerStatus;
    pool.query(query.change_owner_admin, [ownerStatus, ownerId], (error, result) => {
        if (error) {
            res.json(Response.error("修改业主审核状态失败", null));
            logger().error("修改业主审核状态失败: " + error);
        } else {
            pool.query(query.select_ownerID_admin, ownerId, (error, result) => {
                if (error) {
                    res.json(Response.error("修改业主审核状态失败", null));
                    logger().error("修改业主审核状态失败: " + error);
                    return;
                } else {
                    const {
                        user_id,
                        openid
                    } = JSON.parse(JSON.stringify(result[0]));
                    const time = sd.format(new Date(), 'YYYY-MM-DD HH:mm');
                    sendSystemMessage_owner_admin(user_id, {
                        openid,
                        handler: "管理员",
                        message: "您有一则业主审核申请已处理",
                        time,
                        ownerId
                    });
                    res.json(Response.success("修改业主审核状态成功", null));
                }
            });
        }
    });
})

//删除访客申请
router.post('/ownerDelete', function (req, res) {
    const ownerId = req.body.ownerId;
    pool.query(query.delete_owner_admin, ownerId, (error, result) => {
        if (error) {
            res.json(Response.error("删除业主认证申请失败", null));
            logger().error("删除业主认证申请失败: " + error);
        } else {
            res.json(Response.success("删除业主认证申请成功", null));
        }
    });
})


//系统授权用户
router.get('/userList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_user_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("系统授权用户列表失败", null));
            logger().error("系统授权用户列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("系统授权用户列表成功", data));
        }
    });
})
//搜索系统用户
router.get('/searchUser', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_user_admin_keyWord, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("搜索系统用户列表失败", null));
            logger().error("搜索系统用户列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("搜索系统用户列表成功", data));
        }
    });
})
//系统用户角色筛选
router.get('/searchUser_role', function (req, res) {
    const role = req.query.role;
    pool.query(query.select_user_admin_role, role, (error, result) => {
        if (error) {
            res.json(Response.error("获取用户角色筛选列表失败", null));
            logger().error("获取用户角色筛选列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取用户角色筛选列表成功", data));
        }
    });
})
//修改用户信息
router.post('/changeUserInfo', function (req, res) {
    const userId = req.body.userId;
    const nickName = req.body.nickName;
    const country = req.body.country;
    const province = req.body.province;
    const city = req.body.city;
    const role = req.body.role;
    pool.query(query.update_user_admin_info, [nickName, country, province, city, role, userId], (error, result) => {
        if (error) {
            res.json(Response.error("修改用户信息失败", null));
            logger().error("修改用户信息失败: " + error);
        } else {
            res.json(Response.success("修改用户信息成功", null));
        }
    });
})

//删除用户
router.post('/userDelete', function (req, res) {
    const userId = req.body.userId;
    pool.query(query.delete_user_admin, userId, (error, result) => {
        if (error) {
            res.json(Response.error("删除用户失败", null));
            logger().error("删除用户失败: " + error);
        } else {
            res.json(Response.success("删除用户成功 ", null));
        }
    });
})


module.exports = router;