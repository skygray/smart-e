const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/systemQuery/systemQuery');

//解析token
const {
    token_sign
} = require('../../config/jwt_admin')

//引入时间模块
const sd = require('silly-datetime');

//系统用户登录
router.post('/login', function (req, res) {
    const userName = req.body.userName;
    const passWord = req.body.password;
    pool.query(query.select_user_login, [userName, passWord], (error, result) => {
        if (error) {
            res.json(Response.error("登录失败", null));
            logger().info(`登录失败: ${error}`);
            return;
        } else {
            const {
                count
            } = JSON.parse(JSON.stringify(result[0]));
            console.log(count)
            if (count == 0) {
                res.json(Response.identity_error("账户名或密码错误", null));
            } else {
                const token = token_sign({
                    userName
                });
                res.json(Response.success("登录成功", {
                    token
                }));
            }
        }
    });
})


//获取登录列表(默认降序)
router.get('/logsList', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_logs_admin, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取登录列表失败", null));
            logger().error("获取登录列表失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取登录列表成功", data));
        }
    });
})

//获取登录列表(升序)
router.get('/logsListByAsc', function (req, res) {
    const count = req.query.count ? parseInt(req.query.count) : 10;
    const page = req.query.page ? parseInt((req.query.page)) - 1 : 0;
    pool.query(query.select_logs_admin_asc, [count, page], (error, result) => {
        if (error) {
            res.json(Response.error("获取登录列表(升序)失败", null));
            logger().error("获取登录列表(升序)失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取登录列表(升序)成功", data));
        }
    });
})

//模糊搜索IP
router.get('/searchRepair_logs_ip', function (req, res) {
    const keywords = req.query.keywords;
    pool.query(query.select_logs_admin_ip, keywords, (error, result) => {
        if (error) {
            res.json(Response.error("获取登录日志搜索IP失败", null));
            logger().error("获取登录日志搜索IP失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取登录日志搜索IP成功", data));
        }
    });
})

//删除登录记录
router.post('/deleteLogs', function (req, res) {
    const logsId = req.body.logsId;
    pool.query(query.delete_logs, logsId, (error, result) => {
        if (error) {
            res.json(Response.error("删除登录记录错误", null));
            logger().error(`删除登录记录错误: ${error}`);
            return;
        } else {
            res.json(Response.success("删除登录记录成功", null));
        }
    });
})


module.exports = router;