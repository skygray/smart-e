const express = require('express');
let router = express.Router();

//日志
const {
    logger
} = require('../../config/logger');

const Response = require('../../utils/response')

//引入数据库连接池
const pool = require('../../config/mysql');
//引入数据库语句
const query = require('../../db/noticeQuery/noticeQuery')

router.get('/', function (req, res) {
    pool.query(query.select_notive, (error, result) => {
        if (error) {
            logger().error("获取公告失败: " + error);
        } else {
            const data = JSON.parse(JSON.stringify(result));
            res.json(Response.success("获取公告成功", data));
        }

    });
})

module.exports = router;