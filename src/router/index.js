//用户模块路由
const userRouter = require('./userRouter/userRouter');

//公告模块路由
const noticeRouter = require('./noticeRouter/noticeRouter');

//公共留言板模块路由
const messRouter = require('./messRouter/messRouter');

//维修模块路由
const repairRouter = require('./repairRouter/repairRouter');

//租赁模块路由
const leaseRouter = require('./leaseRouter/leaseRouter');

//投诉模块路由
const complaintRouter = require('./complaintRouter/complaintRouter');

//租客模块路由
const tenantRouter = require('./tenantRouter/tenantRouter');

//小纸条模块路由
const noteRouter = require('./noteRouter/noteRouter');

//访客模块路由
const visitorRouter = require('./visitorRouter/visitorRouter');


//管理端
const adminRouter_visitor = require('./visitorRouter/adminRouter'); //访客管理
const adminRouter_user = require('./userRouter/adminRouter'); //用户管理
const adminRouter_mess = require('./messRouter/adminRouter'); //公共留言板
const adminRouter_repair = require('./repairRouter/adminRouter'); //公共留言板
const adminRouter_notice = require('./noticeRouter/adminRouter'); //公告
const adminRouter_complaint = require('./complaintRouter/adminRouter'); //投诉
const adminRouter_lease = require('./leaseRouter/adminRouter'); //租赁
const adminRouter_system = require('./systemRouter/systemRouter'); //系统

module.exports = {
    userRouter,
    noticeRouter,
    messRouter,
    repairRouter,
    leaseRouter,
    complaintRouter,
    tenantRouter,
    noteRouter,
    visitorRouter,
    adminRouter_visitor,
    adminRouter_user,
    adminRouter_mess,
    adminRouter_repair,
    adminRouter_notice,
    adminRouter_complaint,
    adminRouter_lease,
    adminRouter_system
}