//日志
const {
    logger
} = require('../config/logger');

//引入数据库连接池
const pool = require('../config/mysql');
//引入数据库语句
const queryLease = require('../db/leaseQuery/leaseQuery');

//解析token
const {
    token_sign,
    token_verify
} = require('../config/jwt')

//引入时间模块
const sd = require('silly-datetime');

//定义聊天房间数组
let hashName = new Map();

//引入租赁聊天
const leaseChat = require('../chat/leaseChat');

function initChat(io) {
    io.on('connection', function (socket) {
        console.log(token_sign({
            userId: 3,
            openId: "oYqBt5IfVdvXMIAHlcxgQ28C-mMQ",
            role: 1
        }))
        console.log(token_sign({
            userId: 1,
            openId: "oYqBt5IfVdvXMIAHlcxgQ28C-mMQ",
            role: 1
        }))
        logger().info(`用户: ${socket.id}进入了小程序`);
        //开启聊天功能获取未读信息
        socket.on('openChat', function (data) {
            const token = data.token;
            const {
                userId
            } = token_verify(token);
            hashName.set(userId, socket.id); // 储存上线的用户
            //初始化未读消息
            pool.query(queryLease.isnotReading_chat, userId, (error, result) => {
                if (error) {
                    logger().error("获取未读消息失败: " + error);
                    return;
                } else {
                    const data = JSON.parse(JSON.stringify(result));
                    if (hashName.has(userId)) {
                        const toSocket = hashName.get(userId);
                        io.to(toSocket).emit('Unmessage', data)
                    }
                }
            });
        });
        //开启监听租赁实时聊天
        leaseChat(io, hashName, socket);
        // 当关闭连接后触发 disconnect 事件
        socket.on('disconnect', function () {
            logger().info(`用户: ${socket.id}离开了小程序`);
        });
    });
}

module.exports = initChat;