 const whiteList = [
     '/user/login', //用户登录
     '/lease/buildings', //获取栋数数据
     '/lease/house', //获取房号数据
     '/messBord/messList', //获取公告留言板
     '/notice', //获取公告信息
     '/lease/leaseList', //获取租赁信息,
     '/lease/leaseList_BetweenPrice', //获取租赁信息(价格区间筛选)
     '/note/detailed', //获取小纸条详细信息

     //放行管理端api
     '/admin/system/login', //管理员登录
     '/admin/repair/repairList',
     '/admin/repair/repairListByAsc',
     '/admin/repair/searchRepair_repair_title',
     '/admin/repair/searchRepair_repair_describe',
     '/admin/repair/changeStatus',
     '/admin/repair/deleteRepair',
     '/admin/messBord/messList',
     '/admin/messBord/messListByAsc',
     '/admin/messBord/searchMessBord_auther',
     '/admin/messBord/searchMessBord_content',
     '/admin/messBord/searchMessBord_status',
     '/admin/messBord/changeStatus',
     '/admin/messBord/changeIndex',
     '/admin/messBord/deleteMess',
     '/admin/notice/noticeList',
     '/admin/notice/noticeListByAsc',
     '/admin/notice/searchNotice_title',
     '/admin/notice/searchNotice_describe',
     '/admin/notice/searchNotice_status',
     '/admin/notice/changeStatus',
     '/admin/notice/changeIndex',
     '/admin/notice/deleteNotice',
     '/admin/user/ownerList',
     '/admin/user/searchOwner',
     '/admin/user/changeOwnerStatus',
     '/admin/user/ownerDelete',
     '/admin/user/userList',
     '/admin/user/searchUser',
     '/admin/user/searchUser_role',
     '/admin/user/changeUserInfo',
     '/admin/user/userDelete',
     '/admin/complaint/complaintList',
     '/admin/complaint/complaintListByAsc',
     '/admin/complaint/searchComplaint_title',
     '/admin/complaint/searchComplaint_describe',
     '/admin/complaint/searchComplaint_status',
     '/admin/complaint/changeStatus',
     '/admin/complaint/deleteComplaint',
     '/admin/lease/leaseList',
     '/admin/lease/leaseListByAsc',
     '/admin/lease/searchLease_title',
     '/admin/lease/searchLease_describe',
     '/admin/lease/searchLease_status',
     '/admin/lease/changeStatus',
     '/admin/lease/changeIndex',
     '/admin/lease/deleteLease',
     '/admin/visitor/visitorList',
     '/admin/visitor/visitorListByAsc',
     '/admin/visitor/searchVisitor',
     '/admin/visitor/changeStatus',
     '/admin/visitor/delete',
     '/admin/system/logsList',
     '/admin/system/logsListByAsc',
     '/admin/system/searchRepair_logs_ip',
     '/admin/system/deleteLogs',
     '/admin/notice/addNotice',
     "/admin/lease/changeInfo"
 ];

 module.exports = whiteList;