class Response {
    constructor(code, message, data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}

const success = function (message, data) {
    return new Response(200, message, data);
}

const error = function (message, data) {
    return new Response(500, message, data);
}

const identity_none = function (data) {
    return new Response(401, "请登陆后操作", data);
}

const identity_error = function (data) {
    return new Response(401, "身份验证失败", data);
}

const identity_success = function (data) {
    return new Response(200, "身份验证成功", data);
}

const param_error = function (data) {
    return new Response(400, "请求参数错误", data);
}

module.exports = {
    success,
    error,
    identity_none,
    identity_error,
    identity_success,
    param_error
}