var getClientIp = function (req) {
    return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress || '';
};

exports.clientIp = function (req) {
    let ip = getClientIp(req).match(/\d+.\d+.\d+.\d+/);
    ip = ip ? ip.join('.') : null;
    return ip;
}