const fs = require('fs');

//引入日志
const {
    logger
} = require('../config/logger');

function storageFile(req, file) {
    const ht = req.protocol; // 获取协议（http或者https）
    const host = req.headers.host; // 获取域名和端口号
    const uploadDir = '../../../upload/repair'
    try {
        let newPath = uploadDir + "/" + file.originalFilename
        fs.renameSync(file.path, newPath)
        const url = ht + "://" + host + uploadDir + file.originalFilename
        return url;
    } catch (error) {
        logger().error("存储文件失败: " + b.errmsg);
        return err;
    }
}

module.exports = storageFile;