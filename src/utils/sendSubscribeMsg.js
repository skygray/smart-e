//引入微信配置
const wx = require('../config/wx');

//引入日志
const {
    logger
} = require('../config/logger');

//处理node request请求
const request = require('request');

//认证审核订阅消息(业主处理)
function sendSubscribeMsg_auth(accessToken, openid, applicant, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "YsB3IPfxKBaqdW6FIFJbv6_6whB5Ji5zCuLJK5xIFkQ",
            page: `/pages/mine/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                thing4: {
                    value: `${applicant}`
                },
                thing8: {
                    value: `租客认证信息处理`
                },
                time6: {
                    value: `${time}`
                },
                phrase1: {
                    value: `审核中`
                }
            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送租赁审核订阅消息失败 ");
                logger().error("推送租赁审核订阅消息失败: " + error);
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送租赁审核订阅消息成功');
                } else {
                    reject('推送租赁审核订阅消息失败 ' + b.errmsg);
                    logger().error("推送租赁审核订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

//小纸条订阅消息
function sendSubscribeMsg_note(accessToken, openid, sender, content, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "_zvMw7oHGSVIp66n3HU-ESiKmMGtfwX9w1vkDPc4cmw",
            page: `/pages/Subpages/noteHistory/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                thing5: {
                    value: `${sender}`
                },
                thing6: {
                    value: `${content}`
                },
                time7: {
                    value: `${time}`
                }
            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送小纸条订阅消息失败 ");
                logger().error("推送小纸条订阅消息失败: " + error);
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送小纸条订阅消息成功');
                } else {
                    reject('推送小纸条订阅消息失败 ' + b.errmsg);
                    logger().error("推送小纸条订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

//访客申请通知（通知业主）
function sendSubscribeMsg_visitor(accessToken, openid, applicant, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "YsB3IPfxKBaqdW6FIFJbv6_6whB5Ji5zCuLJK5xIFkQ",
            page: `/pages/Subpages/accessControl/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                thing4: {
                    value: `${applicant}`
                },
                thing8: {
                    value: `访客审核通知`
                },
                time6: {
                    value: `${time}`
                },
                phrase1: {
                    value: `待审核`
                }
            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送访客审核订阅消息失败 ");
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送访客审核订阅消息成功');
                } else {
                    reject('推送访客审核订阅消息失败 ' + b.errmsg);
                    logger().error("推送访客审核订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

//业主修改申请通知（通知申请者）
function sendSubscribeMsg_visitor_house(accessToken, openid, handler, status, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "zngnwSVovxdN0brakPo9dBrY757TgH-fs5migdwGxpA",
            page: `/pages/Subpages/visitor/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                name5: {
                    value: `${handler}`
                },
                thing7: {
                    value: `访客申请已受业主处理`
                },
                phrase4: {
                    value: status
                },
                date3: {
                    value: `${time}`
                },

            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送业主审核通知订阅消息失败 ");
                logger().error("推送业主审核通知订阅消息失败: " + error);
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送业主审核通知订阅消息成功');
                } else {
                    reject('推送业主审核通知订阅消息失败 ' + b.errmsg)
                    logger().error("推送业主审核通知订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

//管理员修改访客申请通知（通知申请者）
function sendSubscribeMsg_visitor_admin(accessToken, openid, handler, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "YsB3IPfxKBaqdW6FIFJbv6_6whB5Ji5zCuLJK5xIFkQ",
            page: `/pages/Subpages/visitor/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                thing4: {
                    value: `${handler}`
                },
                thing8: {
                    value: `访客审核通知`
                },
                time6: {
                    value: `${time}`
                },
                phrase1: {
                    value: `已审核`
                }
            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送访客审核订阅消息失败 ");
                logger().error("推送访客审核订阅消息失败: " + error);
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送访客审核订阅消息成功');
                } else {
                    reject('推送访客审核订阅消息失败 ' + b.errmsg)
                    logger().error("推送访客审核订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

//管理员修改业主认证申请通知（通知业主）
function sendSubscribeMsg_owner_admin(accessToken, openid, handler, time) {
    return new Promise((resolve, reject) => {
        let requestData = {
            touser: `${openid}`,
            template_id: "YsB3IPfxKBaqdW6FIFJbv6_6whB5Ji5zCuLJK5xIFkQ",
            page: `pages/Subpages/ownerHistory/index`,
            miniprogram_state: wx.miniprogram_state,
            data: {
                thing4: {
                    value: `${handler}`
                },
                thing8: {
                    value: `业主审核通知`
                },
                time6: {
                    value: `${time}`
                },
                phrase1: {
                    value: `已审核`
                }
            }
        }
        let options = {
            method: 'POST',
            url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${accessToken}`,
            headers: {
                "content-type": "application/json",
            },
            body: JSON.stringify(requestData)
        };
        request(options, (error, response, body) => {
            if (error) { //请求异常时，返回错误信息
                reject("推送业主审核订阅消息失败 ");
                logger().error("推送业主审核订阅消息失败: " + error);
            } else {
                const b = JSON.parse(body);
                if (b.errcode == 0) {
                    resolve('推送业主审核订阅消息成功');
                } else {
                    reject('推送业主审核订阅消息失败 ' + b.errmsg)
                    logger().error("推送业主审核订阅消息失败: " + b.errmsg);
                }
            }
        })
    })
}

module.exports = {
    sendSubscribeMsg_auth,
    sendSubscribeMsg_note,
    sendSubscribeMsg_visitor,
    sendSubscribeMsg_visitor_house,
    sendSubscribeMsg_visitor_admin,
    sendSubscribeMsg_owner_admin
};