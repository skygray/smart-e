const insert_complaint = "insert into smart_complaint(openId,complaint_title,complaint_describe,complaint_status,complaint_date) values (?,?,?,0,?)";

//管理端
//获取投诉列表(默认降序)
const select_complaint_admin = "select smart_complaint.id,smart_user.nickName,smart_user.avatarUrl,smart_complaint.complaint_title,smart_complaint.complaint_describe,smart_complaint.complaint_status,smart_complaint.complaint_date from smart_complaint left join smart_user on smart_complaint.openId=smart_user.openid order by complaint_date desc limit ? offset ?";
//降序
const select_complaint_admin_asc = "select smart_complaint.id,smart_user.nickName,smart_user.avatarUrl,smart_complaint.complaint_title,smart_complaint.complaint_describe,smart_complaint.complaint_status,smart_complaint.complaint_date from smart_complaint left join smart_user on smart_complaint.openId=smart_user.openid order by complaint_date asc limit ? offset ?";
//模糊搜索标题
const select_complaint_admin_title = "select smart_complaint.id,smart_user.nickName,smart_user.avatarUrl,smart_complaint.complaint_title,smart_complaint.complaint_describe,smart_complaint.complaint_status,smart_complaint.complaint_date from smart_complaint left join smart_user on smart_complaint.openId=smart_user.openid where complaint_title like concat('%', ? , '%')";
//模糊搜索详细
const select_complaint_admin_describe = "select smart_complaint.id,smart_user.nickName,smart_user.avatarUrl,smart_complaint.complaint_title,smart_complaint.complaint_describe,smart_complaint.complaint_status,smart_complaint.complaint_date from smart_complaint left join smart_user on smart_complaint.openId=smart_user.openid where complaint_describe like concat('%', ? , '%')";
//状态筛选
const select_status_admin = "select smart_complaint.id,smart_user.nickName,smart_user.avatarUrl,smart_complaint.complaint_title,smart_complaint.complaint_describe,smart_complaint.complaint_status,smart_complaint.complaint_date from smart_complaint left join smart_user on smart_complaint.openId=smart_user.openid where complaint_status=? limit ? offset ?";
//修改状态
const change_complaint_admin = "update smart_complaint set complaint_status=? where id=?";
//删除公告
const delete_complaint = "delete from smart_complaint where id = ?";

module.exports = {
    insert_complaint,
    select_complaint_admin,
    select_complaint_admin_asc,
    select_complaint_admin_title,
    select_complaint_admin_describe,
    select_status_admin,
    change_complaint_admin,
    delete_complaint
}