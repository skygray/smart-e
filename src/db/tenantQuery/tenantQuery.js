const insert_tenant = "insert into smart_tenant(user_id,house_id,tenant_name,tenant_sex,tenant_tel,tenant_email,tenant_contractUrl,tenant_status,tenant_date) values (?,?,?,?,?,?,?,0,?)";

const select_houseOwner_id = "select user_id from smart_house left join smart_household_owner on smart_house.owner_id=smart_household_owner.id where smart_house.id = ?";

const select_tanant = "select * from smart_tenant where id = ?";

const select_tanant_all = "select smart_tenant.id,smart_tenant.user_id,smart_tenant.house_id,smart_tenant.tenant_name,smart_tenant.tenant_sex,smart_tenant.tenant_tel,smart_tenant.tenant_email,smart_tenant.tenant_contractUrl,smart_tenant.tenant_status,smart_tenant.tenant_date from smart_tenant left join smart_house on smart_tenant.house_id=smart_house.id left join smart_household_owner on smart_house.owner_id=smart_household_owner.id where smart_household_owner.user_id = ?";

const select_houseOwner_userId = "select smart_household_owner.user_id from smart_tenant left join smart_house on smart_tenant.house_id=smart_house.id left join smart_household_owner on smart_house.owner_id=smart_household_owner.id where smart_tenant.id = ?";

const change_tanant_status = "update smart_tenant set tenant_status = ? where id = ?";

const select_tanantByuserId = "select * from smart_tenant where user_id = ?";

module.exports = {
    insert_tenant,
    select_houseOwner_id,
    select_tanant,
    select_tanant_all,
    select_houseOwner_userId,
    change_tanant_status,
    select_tanantByuserId
}