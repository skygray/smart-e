const select_mess = "select * from smart_mess where mess_status=1 limit ? offset ?";

const add_mess = "insert into smart_mess(mess_author,mess_avatarUrl,mess_content,mess_date,mess_status) values (?,?,?,?,0)"


//管理端
const select_mess_admin = "select * from smart_mess order by mess_date desc limit ? offset ?";
//降序
const select_mess_admin_asc = "select * from smart_mess order by mess_date asc limit ? offset ?";
//模糊搜索发表者
const select_mess_admin_auther = "select * from smart_mess where mess_author like concat('%', ? , '%')";
//模糊搜索内容
const select_mess_admin_content = "select * from smart_mess where mess_content like concat('%', ? , '%')";
//状态筛选
const select_status_admin = "select * from smart_mess where mess_status=? limit ? offset ?";
//修改状态
const change_mess_admin = "update smart_mess set mess_status=? where id=?";
//判断层级重复
const select_mess_admin_index = "select count(*) as count from smart_mess where mess_index=?";
//修改显示层级
const change_mess_admin_index = "update smart_mess set mess_index=? where id=?";
//删除留言
const delete_mess = "delete from smart_mess where id = ?";

module.exports = {
    select_mess,
    add_mess,
    select_mess_admin,
    select_mess_admin_asc,
    select_mess_admin_auther,
    select_mess_admin_content,
    select_status_admin,
    change_mess_admin,
    select_mess_admin_index,
    change_mess_admin_index,
    delete_mess,

}