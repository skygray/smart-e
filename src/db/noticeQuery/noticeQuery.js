const select_notive = "select id,notice_title,notice_describe,notice_img,notice_top,notice_date from smart_notice where notice_status=1";

const insert_notive = "insert into smart_sysmess(user_id,relation_id,sysMess_title,sysMess_describe,sysMess_status,sysMess_date) values (?,?,?,?,0,?)";

//获取系统未读消息
const select_sysMess = "select * from smart_sysmess where sysMess_status=0 and user_id=?";

//管理端
//获取公告列表(默认降序)
const select_notice_admin = "select * from smart_notice order by notice_date desc limit ? offset ?";
//降序
const select_notice_admin_asc = "select * from smart_notice order by notice_date asc limit ? offset ?";
//模糊搜索标题
const select_notice_admin_title = "select * from smart_notice where notice_title like concat('%', ? , '%')";
//模糊搜索详细
const select_notice_admin_describe = "select * from smart_notice where notice_describe like concat('%', ? , '%')";
//状态筛选
const select_status_admin = "select * from smart_notice where notice_status=? limit ? offset ?";
//添加公告
const insert_notive_admin = "insert into smart_notice(notice_title,notice_describe,notice_img,notice_status,notice_top,notice_date) values (?,?,?,0,?,?)";
//修改状态
const change_notice_admin = "update smart_notice set notice_status=? where id=?";
//判断层级重复
const select_notice_admin_index = "select count(*) as count from smart_notice where notice_top=?";
//修改显示层级
const change_notice_admin_index = "update smart_notice set notice_top=? where id=?";
//删除公告
const delete_notice = "delete from smart_notice where id = ?";

module.exports = {
    select_notive,
    insert_notive,
    select_sysMess,
    select_notice_admin,
    select_notice_admin_asc,
    select_notice_admin_title,
    select_notice_admin_describe,
    select_status_admin,
    insert_notive_admin,
    change_notice_admin,
    select_notice_admin_index,
    change_notice_admin_index,
    delete_notice
}