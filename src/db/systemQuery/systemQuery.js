//用户登录
const select_user_login = "select count(*) as count from smart_admin where admin_username=? and admin_password=?";

//获取登录列表(默认降序)
const select_logs_admin = "select * from smart_login_log order by login_time desc limit ? offset ?"

//获取登录列表(升序)
const select_logs_admin_asc = "select * from smart_login_log order by login_time asc limit ? offset ?";

//模糊搜索IP
const select_logs_admin_ip = "select * from smart_login_log where login_ip like concat('%',?,'%')";

//删除登录记录
const delete_logs = "delete from smart_login_log where id = ?";

module.exports = {
    select_user_login,
    select_logs_admin,
    select_logs_admin_asc,
    select_logs_admin_ip,
    delete_logs
}