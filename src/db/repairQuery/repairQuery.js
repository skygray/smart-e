const insert_repair = "insert into smart_repair(user_id,repair_title,repair_img,repair_describe,repair_tel,repair_type,repair_status,repair_date) values (?,?,?,?,?,?,0,?)";

const select_repair = "select * from smart_repair where user_id = ?"

const select_userId = "select user_id from smart_repair where id = ?";

const delete_repair = "delete from smart_repair where id = ?";

//管理端
//获取维修列表(默认降序)
const select_repair_admin = "select smart_repair.id,smart_user.nickName,smart_user.avatarUrl,smart_repair.repair_title,smart_repair.repair_img,smart_repair.repair_describe,smart_repair.repair_tel,smart_repair.repair_type,smart_repair.repair_status,smart_repair.repair_date from smart_repair left join smart_user on smart_repair.user_id=smart_user.id order by smart_repair.repair_date desc limit ? offset ?"

//获取维修列表(升序)
const select_repair_admin_asc = "select smart_repair.id,smart_user.nickName,smart_user.avatarUrl,smart_repair.repair_title,smart_repair.repair_img,smart_repair.repair_describe,smart_repair.repair_tel,smart_repair.repair_type,smart_repair.repair_status,smart_repair.repair_date from smart_repair left join smart_user on smart_repair.user_id=smart_user.id order by smart_repair.repair_date asc limit ? offset ?";

//模糊搜索标题
const select_repair_admin_title = "select smart_repair.id,smart_user.nickName,smart_user.avatarUrl,smart_repair.repair_title,smart_repair.repair_img,smart_repair.repair_describe,smart_repair.repair_tel,smart_repair.repair_type,smart_repair.repair_status,smart_repair.repair_date from smart_repair left join smart_user on smart_repair.user_id=smart_user.id where smart_repair.repair_title like concat('%',?,'%')";

//模糊搜索详细描述
const select_repair_admin_describe = "select smart_repair.id,smart_user.nickName,smart_user.avatarUrl,smart_repair.repair_title,smart_repair.repair_img,smart_repair.repair_describe,smart_repair.repair_tel,smart_repair.repair_type,smart_repair.repair_status,smart_repair.repair_date from smart_repair left join smart_user on smart_repair.user_id=smart_user.id where smart_repair.repair_describe like concat('%',?,'%')";

//修改维修状态
const change_repair_admin = "update smart_repair set repair_status=? where id=?";

module.exports = {
    insert_repair,
    select_repair,
    select_userId,
    delete_repair,
    select_repair_admin,
    select_repair_admin_asc,
    select_repair_admin_title,
    select_repair_admin_describe,
    change_repair_admin
}