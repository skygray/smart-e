const http = require('http');
const express = require('express');

const serveConfig = require('./src/config/serve');
const router = require('./src/router/index');

const app = express();

// 允许跨域资源共享
const cors = require('cors');
app.use(cors());

//发布静态资源
const path = require('path');
app.use(express.static(path.join(__dirname, './public')))

//处理formdata
const bodyParser = require('body-parser');
app.use(bodyParser.json()); //解析json
app.use(bodyParser.urlencoded({
    extended: false
})); //解析x-www-form-urlencoded

//引入身份拦截器
const Authorization = require('./src/interceptor/authorization');
app.use(Authorization);

//配置路由中间件
app.use("/user", router.userRouter);
app.use("/notice", router.noticeRouter);
app.use("/messBord", router.messRouter);
app.use("/repair", router.repairRouter);
app.use("/lease", router.leaseRouter);
app.use("/complaint", router.complaintRouter);
app.use("/tenant", router.tenantRouter);
app.use("/note", router.noteRouter);
app.use("/visitor", router.visitorRouter);

//引入管理员身份拦截器
const Authorization_admin = require('./src/interceptor/authorization_admin');
app.use(Authorization_admin);
app.use("/admin/visitor", router.adminRouter_visitor);
app.use("/admin/user", router.adminRouter_user);
app.use("/admin/messBord", router.adminRouter_mess);
app.use("/admin/repair", router.adminRouter_repair);
app.use("/admin/notice", router.adminRouter_notice);
app.use("/admin/complaint", router.adminRouter_complaint);
app.use("/admin/lease", router.adminRouter_lease);
app.use("/admin/system", router.adminRouter_system);

//定义套接字聊天依赖
const server = http.createServer(app);
const io = require('socket.io')(server);
const initChat = require('./src/chat/initChat');
const initNotice = require("./src/notice/initNotice");
initChat(io); //引入实时聊太功能聊天'
initNotice(io); //引入实时通知功能

server.listen(serveConfig.port, function () {
    console.log(`Serve is running on port:${serveConfig.port}`);
});